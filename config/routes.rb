Rails.application.routes.draw do
  resources :votations
  resources :drinks
  resources :foods
  resources :materials
  resources :products
  resources :events
  resources :users
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }
  resources :forms
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
