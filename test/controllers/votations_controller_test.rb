require 'test_helper'

class VotationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @votation = votations(:one)
  end

  test "should get index" do
    get votations_url, as: :json
    assert_response :success
  end

  test "should create votation" do
    assert_difference('Votation.count') do
      post votations_url, params: { votation: { date_possible: @votation.date_possible, votes: @votation.votes } }, as: :json
    end

    assert_response 201
  end

  test "should show votation" do
    get votation_url(@votation), as: :json
    assert_response :success
  end

  test "should update votation" do
    patch votation_url(@votation), params: { votation: { date_possible: @votation.date_possible, votes: @votation.votes } }, as: :json
    assert_response 200
  end

  test "should destroy votation" do
    assert_difference('Votation.count', -1) do
      delete votation_url(@votation), as: :json
    end

    assert_response 204
  end
end
