# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(:skip_validation => true, name:"Aopan", email:"aopan@id.uff.br", password: "admin", kind: 0, paying: 1)
User.create(:skip_validation => true, name:"Admin", email:"admin@id.uff.br", password: "admin", kind: 0, paying: 1)
Event.create(name:"ChurrasquIN")
