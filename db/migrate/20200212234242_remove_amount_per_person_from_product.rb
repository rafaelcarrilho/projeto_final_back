class RemoveAmountPerPersonFromProduct < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :amount_per_person, :float
  end
end
