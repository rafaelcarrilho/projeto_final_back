class RemoveEstimatedCostFromEvent < ActiveRecord::Migration[5.2]
  def change
    remove_column :events, :estimated_cost, :float
  end
end
