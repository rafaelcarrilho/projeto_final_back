class AddFundToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :fund, :float, default: 0.00
  end
end
