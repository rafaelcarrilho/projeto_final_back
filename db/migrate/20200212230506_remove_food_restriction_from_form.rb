class RemoveFoodRestrictionFromForm < ActiveRecord::Migration[5.2]
  def change
    remove_column :forms, :food_restriction, :integer
  end
end
