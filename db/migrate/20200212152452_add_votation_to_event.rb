class AddVotationToEvent < ActiveRecord::Migration[5.2]
  def change
    add_reference :events, :votation, foreign_key: true
  end
end
