class AddPaidToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :paid, :integer, default:0
  end
end
