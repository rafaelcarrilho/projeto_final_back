class AddChosenDateToForms < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :chosen_date, :date
  end
end
