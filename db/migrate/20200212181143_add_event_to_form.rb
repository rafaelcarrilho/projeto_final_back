class AddEventToForm < ActiveRecord::Migration[5.2]
  def change
    add_reference :forms, :event, foreign_key: true
  end
end
