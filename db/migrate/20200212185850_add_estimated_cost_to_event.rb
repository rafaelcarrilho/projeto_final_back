class AddEstimatedCostToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :estimated_cost, :float
  end
end
