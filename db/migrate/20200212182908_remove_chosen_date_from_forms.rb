class RemoveChosenDateFromForms < ActiveRecord::Migration[5.2]
  def change
    remove_column :forms, :chosen_date, :integer
  end
end
