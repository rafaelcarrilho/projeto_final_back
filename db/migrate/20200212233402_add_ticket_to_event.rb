class AddTicketToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :ticket, :float, default:0
  end
end
