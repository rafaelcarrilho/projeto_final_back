class RemoveFormFromEvent < ActiveRecord::Migration[5.2]
  def change
    remove_reference :events, :form, foreign_key: true
  end
end
