class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    create_table :foods do |t|
      t.float :amount
      t.integer :kind
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
