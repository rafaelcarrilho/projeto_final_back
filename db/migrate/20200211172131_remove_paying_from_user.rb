class RemovePayingFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :paying, :int
  end
end
