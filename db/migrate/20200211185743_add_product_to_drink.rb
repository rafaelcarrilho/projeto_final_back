class AddProductToDrink < ActiveRecord::Migration[5.2]
  def change
    add_reference :drinks, :product, foreign_key: true
  end
end
