class RemoveDrinkRestrictionFromForm < ActiveRecord::Migration[5.2]
  def change
    remove_column :forms, :drink_restriction, :integer
  end
end
