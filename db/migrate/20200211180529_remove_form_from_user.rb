class RemoveFormFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_reference :users, :form, foreign_key: true
  end
end
