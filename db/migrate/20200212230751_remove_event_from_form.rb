class RemoveEventFromForm < ActiveRecord::Migration[5.2]
  def change
    remove_reference :forms, :event, foreign_key: true
  end
end
