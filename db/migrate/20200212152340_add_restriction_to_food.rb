class AddRestrictionToFood < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :restriction, :integer, default:0
  end
end
