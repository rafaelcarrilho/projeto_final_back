class AddMaximumDateToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :maximum_date, :date
  end
end
