class AddRestrictionToDrink < ActiveRecord::Migration[5.2]
  def change
    add_column :drinks, :restriction, :integer, default:0
  end
end
