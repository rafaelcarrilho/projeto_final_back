class CreateDrinks < ActiveRecord::Migration[5.2]
  def change
    create_table :drinks do |t|
      t.integer :amount
      t.integer :kind

      t.timestamps
    end
  end
end
