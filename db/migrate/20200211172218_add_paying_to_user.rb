class AddPayingToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :paying, :int, default:0
  end
end
