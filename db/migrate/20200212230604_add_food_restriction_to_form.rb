class AddFoodRestrictionToForm < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :food_restriction, :integer, default: 0
  end
end
