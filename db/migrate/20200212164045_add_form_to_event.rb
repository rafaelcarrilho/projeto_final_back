class AddFormToEvent < ActiveRecord::Migration[5.2]
  def change
    add_reference :events, :form, foreign_key: true
  end
end
