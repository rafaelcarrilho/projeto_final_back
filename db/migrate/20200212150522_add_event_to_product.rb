class AddEventToProduct < ActiveRecord::Migration[5.2]
  def change
    add_reference :products, :event, foreign_key: true, default: 1
  end
end
