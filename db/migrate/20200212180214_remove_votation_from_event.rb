class RemoveVotationFromEvent < ActiveRecord::Migration[5.2]
  def change
    remove_reference :events, :votation, foreign_key: true
  end
end
