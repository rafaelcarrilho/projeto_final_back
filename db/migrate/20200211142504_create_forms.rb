class CreateForms < ActiveRecord::Migration[5.2]
  def change
    create_table :forms do |t|
      t.integer :chosen_date
      t.integer :food_restriction
      t.integer :drink_restriction
      t.string :allergy
      t.string :comment

      t.timestamps
    end
  end
end
