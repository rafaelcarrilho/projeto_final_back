class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.date :date_event
      t.integer :paid
      t.string :name
      t.string :location
      t.float :funds
      t.float :ticket

      t.timestamps
    end
  end
end
