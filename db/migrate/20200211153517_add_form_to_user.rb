class AddFormToUser < ActiveRecord::Migration[5.2]
  def change
    add_reference :users, :form, foreign_key: true
  end
end
