class AddPayingToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :paying, :int
  end
end
