class CreateVotations < ActiveRecord::Migration[5.2]
  def change
    create_table :votations do |t|
      t.date :date_possible
      t.integer :votes, default: 0
      t.timestamps
    end
  end
end
