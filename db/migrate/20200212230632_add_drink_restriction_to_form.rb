class AddDrinkRestrictionToForm < ActiveRecord::Migration[5.2]
  def change
    add_column :forms, :drink_restriction, :integer, default: 0
  end
end
