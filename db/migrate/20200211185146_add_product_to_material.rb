class AddProductToMaterial < ActiveRecord::Migration[5.2]
  def change
    add_reference :materials, :product, foreign_key: true
  end
end
