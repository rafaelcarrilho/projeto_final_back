class AddEstimateCostToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :estimated_cost, :float, default: 0
  end
end
