class RemovePaidFromEvents < ActiveRecord::Migration[5.2]
  def change
    remove_column :events, :paid, :integer
  end
end
