class RemoveFundsFromEvents < ActiveRecord::Migration[5.2]
  def change
    remove_column :events, :funds, :float
  end
end
