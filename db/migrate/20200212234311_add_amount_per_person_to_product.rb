class AddAmountPerPersonToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :amount_per_person, :float, default: 1
  end
end
