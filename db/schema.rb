# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_12_234311) do

  create_table "drinks", force: :cascade do |t|
    t.integer "amount"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "product_id"
    t.integer "restriction", default: 0
    t.index ["product_id"], name: "index_drinks_on_product_id"
  end

  create_table "events", force: :cascade do |t|
    t.date "date_event"
    t.string "name"
    t.string "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "paid", default: 0
    t.float "fund", default: 0.0
    t.date "maximum_date"
    t.float "ticket", default: 0.0
    t.float "estimated_cost", default: 0.0
  end

  create_table "foods", force: :cascade do |t|
    t.float "amount"
    t.integer "kind"
    t.integer "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restriction", default: 0
    t.index ["product_id"], name: "index_foods_on_product_id"
  end

  create_table "forms", force: :cascade do |t|
    t.string "allergy"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.date "chosen_date"
    t.integer "food_restriction", default: 0
    t.integer "drink_restriction", default: 0
    t.integer "event_id", default: 1
    t.index ["event_id"], name: "index_forms_on_event_id"
    t.index ["user_id"], name: "index_forms_on_user_id"
  end

  create_table "jwt_blacklists", force: :cascade do |t|
    t.string "jti", null: false
    t.index ["jti"], name: "index_jwt_blacklists_on_jti"
  end

  create_table "materials", force: :cascade do |t|
    t.integer "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "product_id"
    t.index ["product_id"], name: "index_materials_on_product_id"
  end

  create_table "products", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id", default: 1
    t.float "amount_per_person", default: 1.0
    t.index ["event_id"], name: "index_products_on_event_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "paying", default: 0
    t.integer "kind", default: 1
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "votations", force: :cascade do |t|
    t.date "date_possible"
    t.integer "votes", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
