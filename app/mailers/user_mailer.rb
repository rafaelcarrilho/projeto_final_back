class UserMailer < ApplicationMailer
    def confirmed_payment_email
        @user = params[:user]
        
        mail(to: "01tpaa.br@gmail.com", subject: "compra confirmada") do |format|
            format.text do
                render :text => 'Compra de ingresso confirmada'
            end
        end
    end
end
