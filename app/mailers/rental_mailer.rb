class RentalMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.rental_mailer.confirmation.subject
  #

class RentalMailer < ApplicationMailer
  before_action :load_rental
 
  def confirmation
    mail to: @email, subject: "Confirmation"
  end
 
  private
 
  def load_rental
    @email = params[:email]
  end
end
end
