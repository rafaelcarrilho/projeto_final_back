class ApplicationMailer < ActionMailer::Base
  default from: '01tpaa.br@gmail.com'
  layout 'mailer'
end
