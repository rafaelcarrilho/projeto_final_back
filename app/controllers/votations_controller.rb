class VotationsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_votation, only: [:show, :update, :destroy]

  # GET /votations
  def index
    @votations = Votation.all

    render json: @votations
  end

  # GET /votations/1
  def show
    render json: @votation
  end

  # POST /votations
  def create
    @votation = Votation.new(votation_params)

    if @votation.save
      render json: @votation, status: :created, location: @votation
    else
      render json: @votation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /votations/1
  def update
    if @votation.update(votation_params)
      render json: @votation
    else
      render json: @votation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /votations/1
  def destroy
    @votation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_votation
      @votation = Votation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def votation_params
      params.require(:votation).permit(:date_possible, :votes)
    end
end
