class FormsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource
  before_action :set_form, only: [:show, :update, :destroy]


  # GET /forms
  def index
    @forms = Form.all

    render json: @forms
  end

  # GET /forms/1
  def show
    render json: @form
  end

  # POST /forms
  def create
    @params = form_params()
    @params[:user_id] = current_user.id
    @form = Form.new(@params)

    if @form.save
      render json: @form, status: :created, location: @form
    else
      render json: @form.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /forms/1
  def update
    if @form.update(form_params)
      render json: @form
    else
      render json: @form.errors, status: :unprocessable_entity
    end
  end

  # DELETE /forms/1
  def destroy
    @form.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_form
      @form = Form.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def form_params
      params.require(:form).permit(:chosen_date, :food_restriction, :drink_restriction, :allergy, :comment, :user_id, :event_id )
    end
end
