class User < ApplicationRecord

  attr_accessor :skip_validation
  
  has_one :form

  validates :name, :email, presence: true

  validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }

  #Impedindo criacao de usuario com tipo administrador
  #Impedindo enviar usuario que ja pagou
  #Mandando email de criacao de conta/ sem confirmacao
  after_create :acount_create_email
  validate :cant_create_admin
  before_create :couldnt_pay
  before_update :paid_update

  private

  def acount_create_email
    ActionMailer::Base.mail(from: "railsteste01@gmail.com", to: email, subject: "Conta registrada", body: "Conta criada").deliver_now
  end
  
  def paid_update
    if paying == "paid"
      @event = Event.find_by!(id: 1)
      @event.update(paid: @event.paid + 1)
      @event.update(fund: @event.fund + @event.ticket)

      #Mandando email de confirmacao de pagamento
      ActionMailer::Base.mail(from: "railsteste01@gmail.com", to: email, subject: "Compra confirmada", body: "Compra de ingresso confirmada").deliver_now
    end
  
  end


  
  enum kind: [
    :admin,
    :member,
    :ex_member,
  ]

  enum paying: [
    :not_paid,
    :paid,
  ]

  devise :database_authenticatable,
  :jwt_authenticatable,:registerable,
  jwt_revocation_strategy: JwtBlacklist
  
  def cant_create_admin
    unless skip_validation
      if kind == "admin"
        errors.add(:kind, "cannot create ADMIN")
      end
    end
  end

  def couldnt_pay
    unless skip_validation
      if paying == "paid"
        errors.add(:paying, "This user couldn't have payed")
      end
    end
  end
  
end

