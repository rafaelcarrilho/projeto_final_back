class Product < ApplicationRecord
    belongs_to :event
    has_one :material
    has_one :food
    has_one :drink

    validates :name, :price, :category, presence: true

    enum category: [:material, :food, :drink]

    after_create :createObject
    def createObject
      
      if category == "material"
        Material.create!(product_id: id, amount: 0)
      elsif category == "food"
        Food.create!(product_id: id, amount: 0)
      elsif category == "drink"
        Drink.create!(product_id: id, amount: 0)
      end

    end
  
end
