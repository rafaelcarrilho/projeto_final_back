class Form < ApplicationRecord
    
    belongs_to :event
    validate :possible_date, :can_have_only_one
    
    belongs_to :user

    after_create :add_votate
    


    enum food_restriction: [:no_food_restriction, :vegetarian, :vegan]
    enum drink_restriction: [:no_drink_restriction, :non_alcoholic]

    
    after_create :increase_products

    private
    
    def increase_products

        #Pegando tamanho das tabelas
        #sizeFoodTable = Food.count
        #sizeMaterialTable = Material.count
        #sizeDrinkTable = Drink.count

        #Aumentando Quantidade de comida pelo enviio de formularios
        #Sem restricao aumenta a quantidade em todas as comidas
        #Vegetariano aumenta quantidade em comidas vegetarianas e veganas
        #Vegano aumenta quantidade em comidas veganas
        if food_restriction == "no_food_restriction"
            Food.find_each do |food|
                #Quantidade atual de comida
                currAmount = food.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(food.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                food.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end

        elsif food_restriction == "vegetarian"
            Food.where(restriction: "vegetarian").find_each do |food|
                #Quantidade atual de comida
                currAmount = food.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(food.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                food.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end

            Food.where(restriction: "vegan").find_each do |food|
                #Quantidade atual de comida
                currAmount = food.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(food.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                food.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end

        elsif food_restriction == "vegan"
            Food.where(restriction: "vegan").find_each do |food|
                #Quantidade atual de comida
                currAmount = food.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(food.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                food.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end
        end


        #Aumentando Quantidade de Bebidas
        #Sem restricao aumenta quantidade de todas as bebidas
        #Nao alcoolico aumenta quantidade de bebidas nao alcoolicas
        if drink_restriction == "no_drink_restriction"
            Drink.find_each do |drink|
                #Quantidade atual de comida
                currAmount = drink.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(drink.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                drink.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end

        elsif drink_restriction == "non_alcoholic"
            Drink.where(restriction: "non_alcoholic").find_each do |drink|
                #Quantidade atual de comida
                currAmount = drink.amount
                #Pegando o produto que se refere a essa comida
                currProduct = Product.find(drink.product_id)
                productAmountPerPerson = currProduct.amount_per_person
                #Atualizando quantidade de comida
                drink.update_attribute(:amount, (currAmount + productAmountPerPerson))
            end
        end
        #Aumentando QUantidade de materiais
        Material.find_each do |material|
            #Quantidade atual de comida
            currAmount = material.amount
            #Pegando o produto que se refere a essa comida
            currProduct = Product.find(material.product_id)
            productAmountPerPerson = currProduct.amount_per_person
            #Atualizando quantidade de comida
            material.update_attribute(:amount, (currAmount + productAmountPerPerson))
        end
        
    end
    private
 
    def possible_date
        @dates_votation = Votation.all
        @dates_votation.each do |i|
            
            if chosen_date == i.date_possible
                
                return
            end 
        end    
        errors.add(:chosen_date, "this isnt a elegible date")
    end

    def add_votate
        @date_to_vote= Votation.find_by!(date_possible: chosen_date) 
        @date_to_vote.update(votes: @date_to_vote.votes + 1 )
    end

    #Um usuario so pode mandar 1 formulario
    def can_have_only_one
        anotherForm = Form.find_by(user_id: user_id)
        if anotherForm != nil
            errors.add(:user_id, "You cant send more than one form")
        end
    end

end
