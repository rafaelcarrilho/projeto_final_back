class Food < ApplicationRecord
  belongs_to :product

  enum restriction: [:no_restriction, :vegetarian, :vegan]
  enum kind: [:meat, :sides, :dessert]

  after_create :update_estimated_cost

    private
    def update_estimated_cost
        #Referenciando evento
        evento = Event.find(1)
        #Preco total desse conjunto de produtos
        currProduct = Product.find(product_id)
        newAmount = amount * currProduct.price
        #Custo total antigo
        oldAmount = evento.estimated_cost
        #Atualizando o custo total
        evento.update_attribute(:estimated_cost, (oldAmount + newAmount))
    end
end
